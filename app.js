import React from 'react';
import AmidoReactResponsiveImage from './src/AmidoReactResponsiveImage.jsx';

var demo1 = {
  availableWidths: [200, 400, 600, 800],
  url: 'http://placehold.it/{width}',
  alt: 'Demo1 test'
};

var demo2 = {
  availableWidths: {
    200: '200/200/abstract',
    400: '400/400/animals',
    600: '600/600/food',
    800: '800/800/city'
  },
  url: 'http://lorempixel.com/{width}',
  alt: 'Demo2 test',
  classNames: {
    'test': true, // Should display.
    'test2': false // Should not display.
  }
};

React.render(
  <AmidoReactResponsiveImage
    url={demo1.url}
    alt={demo1.alt}
    availableWidths={demo1.availableWidths}/>,
  document.getElementById('demo1')
);

React.render(
  <AmidoReactResponsiveImage
    url={demo2.url}
    alt={demo2.alt}
    availableWidths={demo2.availableWidths}
    classNames={demo2.classNames}/>,
  document.getElementById('demo2')
);
