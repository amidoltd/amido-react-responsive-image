/**
 * Copyright (c) Amido Ltd. All rights reserved.
 */
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

// eslint-disable-line no-unused-vars

var _imagerJs = require('imager.js');

var _imagerJs2 = _interopRequireDefault(_imagerJs);

// eslint-disable-line no-unused-vars

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var AmidoReactResponsiveImage = _react2['default'].createClass({
  displayName: 'AmidoReactResponsiveImage',

  propTypes: {
    classNames: _react2['default'].PropTypes.object,
    url: _react2['default'].PropTypes.string.isRequired,
    alt: _react2['default'].PropTypes.string.isRequired,
    availableWidths: _react2['default'].PropTypes.any.isRequired
  },

  getDefaultProps: function getDefaultProps() {
    return {
      classNames: {}
    };
  },

  componentDidMount: function componentDidMount() {
    var placeholderElems = [_react2['default'].findDOMNode(this.refs.primaryElement)],
        ImagerObj = new _imagerJs2['default'](placeholderElems, { // eslint-disable-line no-unused-vars
      availableWidths: this.props.availableWidths
    });
  },

  render: function render() {
    var classes = (0, _classnames2['default'])(this.props.classNames);

    return _react2['default'].createElement('div', { ref: 'primaryElement', className: 'delayed-image-load', 'data-class': classes, 'data-src': this.props.url, 'data-alt': this.props.alt });
  }
});

exports['default'] = AmidoReactResponsiveImage;
module.exports = exports['default'];
//# sourceMappingURL=index.js.map
