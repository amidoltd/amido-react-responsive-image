(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['exports', 'module', 'react', 'imager.js', 'classnames'], factory);
  } else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module, require('react'), require('imager.js'), require('classnames'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, mod, global.React, global.Imager, global.classNames);
    global.AmidoReactResponsiveImage = mod.exports;
  }
})(this, function (exports, module, _react, _imagerJs, _classnames) {
  /**
   * Copyright (c) Amido Ltd. All rights reserved.
   */
  'use strict';

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

  var _React = _interopRequireDefault(_react);

  // eslint-disable-line no-unused-vars

  var _Imager = _interopRequireDefault(_imagerJs);

  // eslint-disable-line no-unused-vars

  var _classNames = _interopRequireDefault(_classnames);

  var AmidoReactResponsiveImage = _React['default'].createClass({
    displayName: 'AmidoReactResponsiveImage',

    propTypes: {
      classNames: _React['default'].PropTypes.object,
      url: _React['default'].PropTypes.string.isRequired,
      alt: _React['default'].PropTypes.string.isRequired,
      availableWidths: _React['default'].PropTypes.any.isRequired
    },

    getDefaultProps: function getDefaultProps() {
      return {
        classNames: {}
      };
    },

    componentDidMount: function componentDidMount() {
      var placeholderElems = [_React['default'].findDOMNode(this.refs.primaryElement)],
          ImagerObj = new _Imager['default'](placeholderElems, { // eslint-disable-line no-unused-vars
        availableWidths: this.props.availableWidths
      });
    },

    render: function render() {
      var classes = (0, _classNames['default'])(this.props.classNames);

      return _React['default'].createElement('div', { ref: 'primaryElement', className: 'delayed-image-load', 'data-class': classes, 'data-src': this.props.url, 'data-alt': this.props.alt });
    }
  });

  module.exports = AmidoReactResponsiveImage;
});
//# sourceMappingURL=index.js.map
