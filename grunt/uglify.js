'use strict';

module.exports = {
  dist: {
    files: {
      'dist/index.min.js': ['dist/index.js']
    }
  }
};
