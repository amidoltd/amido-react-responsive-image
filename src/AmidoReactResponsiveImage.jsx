/**
 * Copyright (c) Amido Ltd. All rights reserved.
 */
import React from 'react'; // eslint-disable-line no-unused-vars
import Imager from 'imager.js'; // eslint-disable-line no-unused-vars
import classNames from 'classnames';

var AmidoReactResponsiveImage = React.createClass({
  propTypes: {
    classNames: React.PropTypes.object,
    url: React.PropTypes.string.isRequired,
    alt: React.PropTypes.string.isRequired,
    availableWidths: React.PropTypes.any.isRequired
  },

  getDefaultProps: function() {
    return {
      classNames: {}
    };
  },

  componentDidMount: function() {
    var placeholderElems = [React.findDOMNode(this.refs.primaryElement)],
        ImagerObj = new Imager(placeholderElems, { // eslint-disable-line no-unused-vars
          availableWidths: this.props.availableWidths
        });
  },

  render: function() {
    var classes = classNames(this.props.classNames);

    return (
      <div ref="primaryElement" className="delayed-image-load" data-class={classes} data-src={this.props.url} data-alt={this.props.alt}/>
    );
  }
});

export default AmidoReactResponsiveImage;
