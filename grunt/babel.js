'use strict';

module.exports = {
  options: {
    sourceMap: true
  },
  lib: {
    files: {
      'lib/index.js': 'src/AmidoReactResponsiveImage.jsx'
    }
  },
  dist: {
    options: {
      modules: 'umd'
    },
    files: {
      'dist/index.js': 'src/AmidoReactResponsiveImage.jsx'
    }
  }
};
